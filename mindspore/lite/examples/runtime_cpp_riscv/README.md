# 构建与运行

- 环境要求
    - 系统环境：Linux x86_64，推荐使用Ubuntu 18.04.02LTS
    - 编译依赖：
        - [CMake](https://cmake.org/download/) >= 3.18.3
        - [riscv64-unknown-linux-gnu-gcc](https://occ.t-head.cn/community/download?id=3853789384499601408) == riscv64-linux-x86_64-20201104
        - [MindSpore Lite 模型推理框架](https://www.mindspore.cn/lite/docs/zh-CN/master/use/build.html) == 1.3.0
        - [Git](https://git-scm.com/downloads) >= 2.28.0

- 编译构建
  1. 手动编译硬件平台为rv64gcv的[MindSpore Lite 模型推理框架](https://www.mindspore.cn/lite/docs/zh-CN/master/use/build.html)，将编译得到的压缩包拷贝到`mindspore/lite/examples/runtime_cpp_riscv/build`目录。 
  2. 在`mindspore/lite/examples/runtime_cpp_riscv`目录下执行build脚本，将能够自动下载相关文件并编译Demo。
      ```bash
      bash build.sh
      ```

  > 若mobilenetv2模型下载失败，请手动下载相关模型文件[mobilenetv2](https://download.mindspore.cn/model_zoo/official/lite/mobilenetv2_openimage_lite/mobilenetv2.ms)，并将其拷贝到`mindspore/lite/examples/runtime_cpp_riscv/model`目录。

- 文件传输

  使用`adb`将`mindspore/lite/examples/runtime_cpp_riscv/output`目录下的`runtime_cpp_demo.tar.gz`压缩包发送到C906 CPU的开发板

  ```shell
  adb push runtime_cpp_demo.tar.gz /data/local/tmp
  ```

- 执行推理

  使用`adb`进入Android Shell命令模式

  ```shell
  adb shell
  ```

  进入压缩包所在的相关目录，并进行解压

  ```shell
  cd /data/local/tmp && tar xzvf runtime_cpp_demo.tar.gz
  ```

  运行示例需要传递两个参数，第一个参数是模型路径，第二个参数是Option，不同的Option将会运行不同的推理流程。

  | option | 流程                        |
  | ------ | --------------------------- |
  | 0      | 基本推理流程                |
  | 1      | 输入维度Resize流程          |
  | 2      | CreateSession简化版接口流程 |
  | 3      | Session并行流程             |
  | 4      | 共享内存池流程              |
  | 5      | 回调运行流程                |

  例如：可以执行以下命令，体验MindSpore Lite推理MobileNetV2模型。

  ```bash
  cd ./runtime_cpp_demo/bin && ./runtime_cpp ../model/mobilenetv2.ms 0
  ```
